const WebSocket = require('ws');

const connectionMessage = JSON.stringify({message: 'Добро пожаловать', username: 'Системное сообщение'});
const systemMessageName = 'Системное сообщение';
let users = [systemMessageName];

const server = new WebSocket.Server({
  port: process.env.PORT || 5000,
  verifyClient: (info, done) => {
    const query = {};
    decodeURI(info.req.url).replace('/?', '').split('&').forEach(el => {
      const separatorInd = el.indexOf('=');
      query[el.slice(0, separatorInd)] = el.slice(separatorInd + 1, el.length);
    })
    const alreadyUser = users.every(el => el !== query.username);

    if(alreadyUser) done(true);
    else done(false, 100, 'Name already used');
  },
});

server.on('connection', (ws, req)=> {
  const query = {};
  decodeURI(req.url).replace('/?', '').split('&').forEach(el => {
    const separatorInd = el.indexOf('=');
    query[el.slice(0, separatorInd)] = el.slice(separatorInd + 1, el.length);
  })

  const { username } = query;
  users.push(username);
  const usersList = users.filter(el => el !== systemMessageName);

  server.clients.forEach(client => {
    if(client.readyState === WebSocket.OPEN) {
      let sendMessage;

      if (client !== ws) {
        const data = {message: `Подключился новый пользователь - ${username}`, username: systemMessageName, usersList}
        sendMessage = JSON.stringify(data);
      } else {
        sendMessage = JSON.stringify({usersList});
      }
      client.send(sendMessage);
    }
  });

  ws.send(connectionMessage);

  ws.on('close', () => {
    users = users.filter(el => el !== username && el !== systemMessageName);
    const data = {message: `Отключился пользователь - ${username}`, username: systemMessageName, usersList: users};
    const sendMessage = JSON.stringify(data);

    server.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(sendMessage);
      }
    });
  });
  
  ws.on('message', function incoming(receiveData) {
    const { message } = JSON.parse(receiveData);
    const data = {message, username};
    const sendMessage = JSON.stringify(data);

    server.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(sendMessage);
      }
    });
  });
});
